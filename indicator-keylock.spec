Name:           indicator-keylock
Version:        3.1.0
Release:        3%{?dist}
Summary:        Lock Keys indicator applet

License:        GPLv3
URL:            https://launchpad.net/indicator-keylock
Source0:        http://archive.ubuntu.com/ubuntu/pool/universe/i/%{name}/%{name}_%{version}.tar.xz
Patch0:         indicator-keylock-3.1.0-icons.patch
Patch1:         indicator-keylock-3.1.0-ldflags.patch
Patch2:         indicator-keylock-3.1.0-gcc10.patch
Patch3:         indicator-keylock-3.1.0-gtkapplication.patch
Patch4:         indicator-keylock-3.1.0-x11.patch

BuildRequires:  desktop-file-utils
BuildRequires:  gcc
BuildRequires:  gnome-common
BuildRequires:  intltool
BuildRequires:  make
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gtk+-3.0)
BuildRequires:  pkgconfig(appindicator3-0.1)
BuildRequires:  pkgconfig(libnotify)
BuildRequires:  pkgconfig(x11)

%description
An indicator that displays the status of the keyboard lock keys.


%prep
%autosetup -p1
gnome-autogen.sh


%build
%configure
%make_build


%install
%make_install
desktop-file-validate %{buildroot}%{_datadir}/applications/%{name}.desktop


%files
%license COPYING
%doc AUTHORS
%{_sysconfdir}/xdg/autostart/indicator-keylock.desktop
%{_bindir}/indicator-keylock
%{_datadir}/applications/indicator-keylock.desktop
%{_datadir}/glib-2.0/schemas/apps.indicators.keylock.gschema.xml
%{_datadir}/icons/hicolor/22x22/status/*-lock-*.png
%{_datadir}/%{name}


%changelog
* Thu Jan 06 2022 Yaakov Selkowitz <yselkowi@redhat.com> - 3.1.0-3
- Use GtkApplication instead of libunique
- Force X11 backend to avoid crash in Wayland sessions

* Tue Nov 30 2021 Yaakov Selkowitz <yselkowi@redhat.com> - 3.1.0-2
- rebuilt

* Tue Jul 21 2020 Yaakov Selkowitz <yselkowi@redhat.com> - 3.1.0-1
- Initial version
